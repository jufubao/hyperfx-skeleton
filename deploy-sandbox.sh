#!/bin/bash
# 部署沙箱/测试环境
workdir=$(cd $(dirname $0); pwd)
cd $workdir
serviceDirs=$(ls ./service)

if [ -f "package.tgz" ]; then
  for dir in $serviceDirs; do
    rm -rf ./service/$dir/*
  done
  tar -xvf ./package.tgz -C ./
fi

for dir in $serviceDirs; do
  rm -rf ./service/$dir/runtime
  mkdir -p ./service/$dir/runtime
  chmod 666 -R ./service/$dir/runtime
done

export DOCKER_DEPLOY_TIME=$(date)
docker stack deploy --compose-file=docker-stack.sandbox.yml sandbox-hyperfx-skeleton