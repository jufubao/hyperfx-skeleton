<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
return [
    'enable' => [
        // 开启服务发现
        'discovery' => true,
        // 开启服务注册
        'register' => false,
    ],
    // 服务提供者相关配置
    'providers' => [
        \Grpc\Base\Common\V1\CommonClient::class => [
            'service' => 'base.common.v1.rpc',
            'options' => [
                'retry_attempts' => 3, // 重试次数
                'retry_interval' => 100, // 重试时Sleep毫秒
                'credentials' => null,
                'send_yield' => true,
            ],
            'metadata' => [],
        ],
    ],
    // 服务驱动相关配置
    'drivers' => [
        'nacos' => [
            'group_name' => 'DEFAULT_GROUP',
            'namespace_id' => env('NACOS_NAMESPACE_ID', 'public'),
            'discovery_interval' => 3,
        ],
    ],
];
