<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
return [
    'default' => [
        'handler' => [
            'class' => Monolog\Handler\StreamHandler::class,
            'constructor' => [
                'stream' => 'php://stdout',
                'level' => env('LOG_LEVEL_DEFAULT', Monolog\Logger::DEBUG),
            ],
        ],
        'formatter' => [
            'class' => env('LOG_FORMATTER_CLASS_DEFAULT', Monolog\Formatter\JsonFormatter::class),
            'constructor' => [
                'dateFormat' => 'Y-m-d H:i:s',
                'allowInlineLineBreaks' => true,
            ],
        ],
    ],
];
