<?php
declare(strict_types=1);
use Hyperf\HttpServer\Router\Router;

// ceshi
Router::addRoute(['get'], '/index', 'App\Controller\IndexController@index', ['handler' => 'Index']);

// 商品属性 - 创建
Router::addRoute(['post'], '/common-admin/v1/demos', 'App\Controller\Admin\DemoController@create', ['handler' => 'Admin_CreateDemo']);

// 商品属性 - 修改
Router::addRoute(['put'], '/common-admin/v1/demos/{id}', 'App\Controller\Admin\DemoController@update', ['handler' => 'Admin_UpdateDemo']);

// 商品属性 - 删除
Router::addRoute(['delete'], '/common-admin/v1/demos/{id}', 'App\Controller\Admin\DemoController@delete', ['handler' => 'Admin_DeleteDemo']);

// 商品属性 - 获取单个
Router::addRoute(['get'], '/common-admin/v1/demos/{id}', 'App\Controller\Admin\DemoController@get', ['handler' => 'Admin_GetDemo']);

// 商品属性 - 列表
Router::addRoute(['get'], '/common-admin/v1/demos', 'App\Controller\Admin\DemoController@lists', ['handler' => 'Admin_ListDemo']);

// 商品属性 - 排序
Router::addRoute(['post'], '/common-admin/v1/demos/{id}/sort', 'App\Controller\Admin\DemoController@sort', ['handler' => 'Admin_SortDemo']);
