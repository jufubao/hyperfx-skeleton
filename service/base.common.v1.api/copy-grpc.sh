#!/bin/bash
workdir=$(cd $(dirname $0); pwd)
cd $workdir

rm -rf grpc/Grpc/*
rm -rf grpc/GPBMetadata/*
cp -f $workdir/../base.common.v1.rpc/grpc/Grpc/BaseStub.php ./grpc/Grpc/

mkdir -p grpc/Grpc/Base/Common
cp -f $workdir/../base.common.v1.rpc/grpc/GPBMetadata/BaseCommonV1.php ./grpc/GPBMetadata/
cp -f -r $workdir/../base.common.v1.rpc/grpc/Grpc/Base/Common ./grpc/Grpc/Base/