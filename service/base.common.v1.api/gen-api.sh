#!/bin/bash
workdir=$(cd $(dirname $0); pwd)
cd $workdir;
goctl api plugin -plugin goctl-hyperfx="hyperfx -package Jufubao" -api base.common.v1.api -dir .
goctl api plugin -plugin goctl-swagger="swagger -filename openapi.json" -api base.common.v1.api -dir ./