<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Controller;

use App\Service\UserService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Annotation\PostMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;
use Hyperfx\Framework\Logger\Logx;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

#[Controller]
class UserController extends AbstractController
{

    #[Inject]
    private UserService $service;

    #[GetMapping(path: "get")]
    public function get(RequestInterface $request, ResponseInterface $response): PsrResponseInterface
    {
        $validator = $this->validationFactory->make(
            $request->query(),
            [
                'user_id' => 'required|numeric|min:1',
            ],
            [
                'user_id.min' => 'user_id no is empty'
            ]
        );
        $params = $this->validate($validator, [
            // 需要过滤成INT类型的可以放这里，其它类型暂时不需要过滤
            'user_id' => 'int'
        ]);
        $result = $this->service->getUser($params);
        return $this->success($response, $result);
    }

    #[PostMapping(path: "create")]
    public function create(RequestInterface $request, ResponseInterface $response): PsrResponseInterface
    {
        $validator = $this->validationFactory->make(
            $request->post(),
            [
                'username' => 'required|min:6,max:255',
                'password' => 'required|string|min:6,max:32',
                'name' => 'required|min:1,max:10',
            ]
        );
        $params = $this->validate($validator);
        $result = $this->service->createUser($params);
        return $this->success($response, $result);
    }
}
