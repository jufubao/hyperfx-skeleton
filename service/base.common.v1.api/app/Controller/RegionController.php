<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Controller;

use App\Service\RegionService;
use Hyperf\Di\Annotation\Inject;
use Hyperf\HttpServer\Annotation\Controller;
use Hyperf\HttpServer\Annotation\GetMapping;
use Hyperf\HttpServer\Contract\RequestInterface;
use Hyperf\HttpServer\Contract\ResponseInterface;

#[Controller]
class RegionController extends AbstractController
{

    #[Inject]
    private RegionService $service;

    #[GetMapping(path: "get")]
    public function get(RequestInterface $request, ResponseInterface $response)
    {
        $validator = $this->validationFactory->make(
            $request->all(),
            [
                'code' => 'required|numeric|min:1',
            ],
            [
                'code.min' => 'code no is empty'
            ]
        );
        $params = $this->validate($validator, [
            // 需要过滤成INT类型的可以放这里，其它类型暂时不需要过滤
            'code' => 'int'
        ]);

        $result = $this->service->getRegion($params);
        return $this->success($response, $result);
    }
}
