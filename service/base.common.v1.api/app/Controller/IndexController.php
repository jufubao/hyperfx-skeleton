<?php
declare(strict_types=1);
namespace App\Controller;

use App\Controller\AbstractController;
use Hyperf\Di\Annotation\Inject;
use App\Service\IndexService;
use App\Request\IndexRequest;
use Hyperf\HttpServer\Contract\ResponseInterface;

class IndexController extends AbstractController
{
    #[Inject]
    private IndexService $service;
	
    /**
     * ceshi
     */
	public function index(IndexRequest $request, ResponseInterface $response)
    {
        $params = $this->validated($request);
        $result = $this->service->index($params, $response);
        return $this->success($response, $result);
    }
	
}