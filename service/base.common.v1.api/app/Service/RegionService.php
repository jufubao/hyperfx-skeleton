<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Service;

use Grpc\Base\Common\V1\CommonClient;
use Grpc\Base\Common\V1\GetRegionRequest;
use Grpc\Base\Common\V1\GetRegionResponse;
use Grpc\Base\Common\V1\GetUserRequest;
use Grpc\Base\Common\V1\GetUserResponse;
use Hyperf\Grpc\StatusCode;
use Hyperf\GrpcClient\Exception\GrpcClientException;

class RegionService
{
    public function getRegion(array $params): array
    {
        /* @var $commonClient CommonClient */
        $commonClient = CommonClient::create();

        $request = new GetRegionRequest();
        $request->setCode($params['code']);

        /* @var $response GetRegionResponse|string */
        // 这个client是协程安全的，可以复用
        [$response, $status] = $commonClient->GetRegion($request);
        if ($status != StatusCode::OK) {
            throw new GrpcClientException($response, $status);
        }

        return [
            'text' => $response->getText(),
        ];
    }
}
