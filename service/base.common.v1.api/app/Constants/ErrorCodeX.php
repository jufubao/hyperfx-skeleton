<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Constants;

use Hyperfx\Framework\Constants\ErrorCodeX as ErrorCodeBase;
use Hyperf\Constants\Annotation\Constants;

#[Constants]
class ErrorCodeX extends ErrorCodeBase
{

}
