<?php
declare(strict_types=1);

namespace Grpc;

use Hyperf\GrpcClient\BaseClient;
use Hyperf\Utils\ApplicationContext;
use Hyperfx\ServiceGovernanceNacos\ServiceClient;

class BaseStub extends BaseClient
{
   /**
     * @return parent
     */
    public static function create()  {
        $container = ApplicationContext::getContainer();
        $client = $container->get(ServiceClient::class);
        return $client->create(get_called_class());
    }
}
