<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
return [
    'grpc' => [
        Hyperf\Tracer\Middleware\TraceMiddleware::class,
        Hyperfx\Framework\Middleware\CoreMiddleware::class,
        Hyperf\Validation\Middleware\ValidationMiddleware::class,
    ],
];
