<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
return [
    'enable' => [
        // 开启服务发现
        'discovery' => false,
        // 开启服务注册
        'register' => true,
    ],
    // 服务消费者相关配置
    'consumers' => [
    ],
    // 服务提供者相关配置
    'providers' => [
    ],
    // 服务驱动相关配置
    'drivers' => [
        'nacos' => [
            'service_name' => env('APP_NAME'),
            'group_name' => 'DEFAULT_GROUP',
            'weight' => 2,
            'namespace_id' => env('NACOS_NAMESPACE_ID', 'public'),
            'protect_threshold' => 0.3, // 浮点数 保护阈值,取值0到1,默认0
            'metadata' => \Hyperf\Utils\Codec\Json::encode([
                'env' => 'dev',
                'group' => 'base',
                'version' => 'v1',
            ]),
            'selector' => null,
            'instance' => [
                'heartbeat' => 3, // 健康检查 0为不检查
                'cluster' => 'DEFAULT',
                'auto_removed' => true,
                'ephemeral' => true, // 是否注册临时实例
            ],
            'cluster' => 'DEFAULT',
            'auto_removed' => true,
        ],
    ],
];
