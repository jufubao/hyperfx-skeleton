<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
return [
    Hyperfx\Framework\Listener\ErrorExceptionHandler::class,
    Hyperfx\ServiceGovernanceNacos\Listener\MainWorkerStartListener::class,
    Hyperfx\ServiceGovernanceNacos\Listener\OnShutdownListener::class,
];
