#!/bin/bash
workdir=$(cd $(dirname $0); pwd)
cd $workdir

../../gowatch run\
   --preCmd "composer run post-autoload-dump" \
   --preCmdIgnoreError=true\
   --cmd "php"\
   --args "bin/hyperf.php, start"\
   --files ".env"\
   --folder "./app/, ./config/"\
   --autoRestart=true