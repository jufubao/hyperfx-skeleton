#!/bin/bash
workdir=$(cd $(dirname $0); pwd)
cd $workdir
rm -rf grpc/Grpc/*
rm -rf grpc/GPBMetadata/*

sh ./vendor/jufubao/hyperfx/scripts/gen-proto.sh base.common.v1.proto