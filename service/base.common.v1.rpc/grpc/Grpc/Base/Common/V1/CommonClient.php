<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Grpc\Base\Common\V1;

/**
 * 用户服务
 */
class CommonClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * 获取一个区域
     * @param \Grpc\Base\Common\V1\GetRegionRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\Base\Common\V1\GetRegionResponse
     */
    public function GetRegion(\Grpc\Base\Common\V1\GetRegionRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/grpc.base.common.v1.Common/GetRegion',
        $argument,
        ['\Grpc\Base\Common\V1\GetRegionResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * ********************************** Users **************************
     *
     * 获取用户
     * @param \Grpc\Base\Common\V1\GetUserRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\Base\Common\V1\GetUserResponse
     */
    public function GetUser(\Grpc\Base\Common\V1\GetUserRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/grpc.base.common.v1.Common/GetUser',
        $argument,
        ['\Grpc\Base\Common\V1\GetUserResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * 创建用户
     * @param \Grpc\Base\Common\V1\CreateUserRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\Base\Common\V1\CreateUserResponse
     */
    public function CreateUser(\Grpc\Base\Common\V1\CreateUserRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/grpc.base.common.v1.Common/CreateUser',
        $argument,
        ['\Grpc\Base\Common\V1\CreateUserResponse', 'decode'],
        $metadata, $options);
    }

}
