<?php
declare(strict_types=1);

namespace App\Database\Redis\Redis;

use Hyperfx\Framework\Redis\RedisModelBase;

class RedisProduct extends RedisModelBase {

    protected string $instance = 'default';

    protected array $mapping = [
        'channel_product' => [
            'key' => 'product:channel:%s',
            'params' => [
                'channel_code',
                'channel_product_id'
            ],
        ],
    ];

    public function setChannelSyncVersion(string $channelCode, string $channelProductId, string $docVersion) {
        $key = $this->generateKey('channel_product', [
            'channel_code' => $channelCode,
            'channel_product_id' => $channelProductId,
        ]);
        $this->client->set($key, $docVersion);
    }

    public function delChannelSyncVersion(string $channelCode, string $channelProductId) {
        $key = $this->generateKey('channel_product', [
            'channel_code' => $channelCode,
            'channel_product_id' => $channelProductId,
        ]);
        $this->client->del($key);
    }
}