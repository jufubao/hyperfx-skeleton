<?php
declare(strict_types=1);

namespace App\Database\Redis\Lock;

use Hyperfx\Framework\Redis\Lock\BaseLock;

class ProductUpdateLock extends BaseLock {

    // 实例
    protected string $instance = 'default';

    // 锁实效时长,单位秒
    protected int $seconds = 5;

    // Lock key, 前缀必须与类名一至
    protected string $key = 'ProductUpdateLock:%u';

    public function __construct(int $productId, int $seconds = 5)
    {
        if ($this->seconds != $seconds) {
            $this->seconds = $seconds;
        }
        $this->step(func_get_args());
    }
}