<?php

declare (strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Database\Mysql;

/**
 * @property int $id
 * @property string $username 用户名
 * @property string $password 密码
 * @property string $name 姓名
 * @property \Carbon\Carbon $created_time
 * @property \Carbon\Carbon $updated_time
 */
class Demo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected ?string $table = 'demo';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected array $fillable = ['id', 'username', 'password', 'name', 'created_time', 'updated_time'];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected array $casts = ['id' => 'integer', 'created_time' => 'datetime', 'updated_time' => 'datetime'];
}