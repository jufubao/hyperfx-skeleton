<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
namespace App\Service;

use Grpc\Base\Common\V1\GetRegionRequest;
use Grpc\Base\Common\V1\GetRegionResponse;
use Hyperf\Grpc\StatusCode;
use Hyperf\GrpcServer\Exception\GrpcException;

class GetRegionService extends BaseService
{
    public function checkRequest(GetRegionRequest $request): void
    {
        if (empty($request->getCode())) {
            throw new GrpcException('code is empty', StatusCode::INVALID_ARGUMENT);
        }
    }

    public function handle(GetRegionRequest $request): GetRegionResponse
    {
        // business logic ..

        $response = new GetRegionResponse();
        $response->setText('Success' . $request->getCode());
        return $response;
    }
}
