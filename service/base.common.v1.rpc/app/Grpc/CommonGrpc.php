<?php
declare(strict_types=1);

namespace App\Grpc;

use Hyperf\HttpServer\Annotation\AutoController;

use App\Service\GetRegionService,
    Grpc\Base\Common\V1\GetRegionRequest,
    Grpc\Base\Common\V1\GetRegionResponse,
    App\Service\GetUserService,
    Grpc\Base\Common\V1\GetUserRequest,
    Grpc\Base\Common\V1\GetUserResponse,
    App\Service\CreateUserService,
    Grpc\Base\Common\V1\CreateUserRequest,
    Grpc\Base\Common\V1\CreateUserResponse;

#[AutoController(prefix: 'grpc.base.common.v1.Common', server: 'grpc')]
class CommonGrpc extends BaseGrpc
{
    
    public function GetRegion(GetRegionRequest $request): GetRegionResponse
    {
        $service = new GetRegionService();
        $service->checkRequest($request);
        return $service->handle($request);
    }

    public function GetUser(GetUserRequest $request): GetUserResponse
    {
        $service = new GetUserService();
        $service->checkRequest($request);
        return $service->handle($request);
    }

    public function CreateUser(CreateUserRequest $request): CreateUserResponse
    {
        $service = new CreateUserService();
        $service->checkRequest($request);
        return $service->handle($request);
    }

}
